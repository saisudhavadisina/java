abstract class Shape{
    int length, breadth;
    abstract float area();
    public Shape(int length, int breadth) {
        super();
        this.length = length;
        this.breadth = breadth;
    }
}

class Rectangle extends Shape{
    public Rectangle(int length, int breadth) {
        super(length, breadth);
    }
    float area() {
        System.out.println("Area of rectangle whose length and breadth are : " + length + ", " + breadth + " is " + length * breadth);
        return 0;
    }
}

class Triangle extends Shape{
    public Triangle(int length, int breadth) {
        super(length, breadth);
    }
    
    float area() {  
        System.out.println("Area of triangle whose base and height are : " + length + ", " + breadth + " is " + length * breadth / 2);
        return 0;   
    }
}

class Circle extends Shape{
    public Circle(int length, int breadth) {
        super(length, breadth);
    }
    
    float area() {
        System.out.println("Area of circle whose radius is " + length + " is " + 3.14 * length * length);
        return 0;
    }
}

public class ShapeArea {
    
    public static void main(String[] args) {
        Rectangle rec = new Rectangle(12, 68);
        rec.area();
        Triangle tri = new Triangle(6, 5);
        
        tri.area();
        Circle cir = new Circle(14, 10);
        cir.area();
    }
    
}

