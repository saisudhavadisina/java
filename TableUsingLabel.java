package jp;

import java.io.*;
import java.util.*;
import java.awt.*;
@SuppressWarnings("serial")
class ExampleTable extends Frame {
	public ExampleTable(){
		setSize(600, 600);
		GridLayout g = new GridLayout(0, 4);
		setLayout(g);
		try {
			FileInputStream fin = new FileInputStream("file:///home/suda/jp/LabAssignment/src/jp/Table.txt" );
			@SuppressWarnings("resource")
			Scanner scan = new Scanner(fin).useDelimiter(",");
			String[] res;
			String word;
			while (scan.hasNextLine()){
				word = scan.nextLine();
				res = word.split(",");
				for (String i : res) {
					add(new Label(i));
				}                   
			}
		} catch (Exception ex) { }
		pack();
		setVisible(true);
	}
}

public class TableUsingLabel {
	public static void main(String[] args) {
		new ExampleTable();
	}
}
