package jp;

import java.awt.*;
import javax.swing.*;
import java.applet.Applet;
import java.awt.event.*;

@SuppressWarnings({ "deprecation", "serial" })
public class IntDivision extends Applet implements ActionListener{ 
	TextField text1,text2,text3; 
	Button but; 
	Label label1, label2, label3, label4; 
	String s; 
	IntDivision i; 
	public void init() { 
		i = this; 
		text1 = new TextField(10);
		text2 = new TextField(10);		
		text3 = new TextField(10);
		label1 = new Label("Enter Num1:"); 
		label2 = new Label("Enter Num2:");
		label3 = new Label("Result:"); 
		label4 = new Label("Division of Two Numbers");
		but = new Button("Divide"); 
		add(label4); 
		add(label1);
		add(text1); 
		add(label2); 
		add(text2);
		add(label3);
		add(text3);
		add(but); 
		but.addActionListener(this); 
	} 
	public void actionPerformed(ActionEvent ae){ 
		try { 
			int num1 = Integer.parseInt(text1.getText());
			int num2 = Integer.parseInt(text2.getText()); 
			s = "" + (num1/num2); 
			text3.setText(s); 
		}catch(ArithmeticException a){ 
			JOptionPane.showMessageDialog(null,"Division by zero is undefined"); 
		}catch(NumberFormatException b){
			JOptionPane.showMessageDialog(null,"Number Format Exception is found"); 
		} 
	}
}