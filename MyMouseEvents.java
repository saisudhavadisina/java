package jp;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

@SuppressWarnings("serial")
class MouseEvents extends JFrame implements MouseListener{
	JLabel lab;
	public MouseEvents(){
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(800, 800);
		setLayout(new FlowLayout());
		lab = new JLabel();
		Font font = new Font("LiberationSerif", Font.BOLD, 15);
		lab.setFont(font);
		lab.setForeground(Color.PINK); 
		lab.setAlignmentX(Component.CENTER_ALIGNMENT);
		lab.setAlignmentY(Component.CENTER_ALIGNMENT);
		add(lab);
		addMouseListener(this);
		setVisible(true);
	}
	public void mouseEntered(MouseEvent mouse){
		lab.setText("Mouse has Entered");
	}
	public void mousePressed(MouseEvent mouse){
		lab.setText("Mouse has Pressed");
	}
	public void mouseClicked(MouseEvent mouse){
		lab.setText("Mouse has Clicked");
	}
	public void mouseReleased(MouseEvent mouse){
		lab.setText("Mouse has Released");
	}
	public void mouseExited(MouseEvent mouse){
		lab.setText("Mouse has Exited");
	}
}
public class MyMouseEvents {
        public static void main(String[] args) {
        	new MouseEvents();
        }
}