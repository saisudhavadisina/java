package jp;

import	java.awt.*;
import	java.awt.event.*;
import	java.applet.*;

@SuppressWarnings({ "serial", "deprecation" })
public class SimpleCalculator extends Applet implements ActionListener {
	String message = " ";
	int x, y, res;
	TextField text;
	Button b[] = new Button[10];
	Button add, diff, mul, div, cls, mod, eq;
	char opt;
	public void init()	{
		text = new TextField(10);
		GridLayout glayout = new GridLayout(4, 5);
		setLayout(glayout);
		for(int i = 0; i < 10; i++) {
			b[i] = new Button("" + i);
		}
		add = new Button("+");
		diff = new Button("-");
		mul = new Button("*");
		div = new Button("/");
		mod = new Button("%");
		cls = new Button("Clear");
		eq = new Button("=");
		text.addActionListener(this);
		add(text);
		for(int i = 0; i < 10; i++){
			add(b[i]);
		}
		add(add);
		add(diff);
		add(mul);
		add(div);
		add(mod);
		add(cls);
		add(eq);
		for(int i = 0; i < 10; i++) {
			b[i].addActionListener(this);
		}
		add.addActionListener(this);
		diff.addActionListener(this);
		mul.addActionListener(this);
		div.addActionListener(this);
		mod.addActionListener(this);
		cls.addActionListener(this);
		eq.addActionListener(this);
	}
	public void actionPerformed(ActionEvent ae) {
		String str = ae.getActionCommand();
		char ch=str.charAt(0);
		if(Character.isDigit(ch))
			text.setText(text.getText()+str);
		else if(str.equals("+")){
			x = Integer.parseInt(text.getText());
			opt = '+';
			text.setText("");
		}
		else if(str.equals("-")) {
			x = Integer.parseInt(text.getText());
			opt = '-';
			text.setText("");
		}
		else if(str.equals("*")){
			x = Integer.parseInt(text.getText());
			opt = '*';
			text.setText("");
		}
		else if(str.equals("/")){
			x = Integer.parseInt(text.getText());
			opt = '/';
			text.setText("");
		}
		else if(str.equals("%")){
			x = Integer.parseInt(text.getText());
			opt = '%';
			text.setText("");
		}
		if(str.equals("="))	{
			y = Integer.parseInt(text.getText());
			if	(opt == '+')
				res = x + y;
			else if (opt == '-')
				res = x - y;
			else if(opt == '*')
				res = x * y;
			else if	(opt == '/'){
				try {
					res = x / y;
				}
				catch(ArithmeticException e){
					System.out.println("Division by zero is undefined");
				}
			}
			else if(opt == '%')
				res = x % y;
			text.setText("" + res);
		}
		if(str.equals("Clear"))		{
			text.setText("");
		}
	}
}